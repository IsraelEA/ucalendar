package pe.edu.uni.fiis.ucalendar.dao.curso;

import pe.edu.uni.fiis.ucalendar.model.Curso;

import java.sql.Connection;
import java.util.List;

public interface CursoDao {
    public Curso agregarcurso(Curso a, Connection b);
    List<Curso> mostrarcursos(Connection b);
}
