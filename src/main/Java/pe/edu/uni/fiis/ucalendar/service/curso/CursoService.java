package pe.edu.uni.fiis.ucalendar.service.curso;

import pe.edu.uni.fiis.ucalendar.model.Curso;

public interface CursoService {
    public Curso agregarcurso(Curso curso);
}
