package pe.edu.uni.fiis.ucalendar.model;


public class Examen {
    private String tipo;
    private String idcurso;
    private String fecha;
    private String hora;
    private String idexamen;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getIdcurso() {
        return idcurso;
    }

    public void setIdcurso(String idcurso) {
        this.idcurso = idcurso;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getIdexamen() {
        return idexamen;
    }

    public void setIdexamen(String idexamen) {
        this.idexamen = idexamen;
    }
}
