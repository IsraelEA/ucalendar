package pe.edu.uni.fiis.ucalendar.controller;

import pe.edu.uni.fiis.ucalendar.model.Usuario;
import pe.edu.uni.fiis.ucalendar.service.SingletonService;
import pe.edu.uni.fiis.ucalendar.util.Json;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UsuarioController", urlPatterns = {"/registro-usuario"})
public class UsuarioController extends HttpServlet {
    //Usuario
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String nombre=req.getParameter("nom");
        String correo=req.getParameter("cor");
        String contaseña=req.getParameter("con");

        Usuario usur1= new Usuario(nombre,correo,contaseña);

        SingletonService.getUsuarioService().agregarUsuario(usur1);
        resp.getWriter().write("Se guardo el usuario correctamente");
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = Json.getJson(req);

        Usuario usuario= Json.getInstance().readValue(data,Usuario.class);

        SingletonService.getUsuarioService().agregarUsuario(usuario);

        Json.envioJson(usuario,resp);

    }
    //Curso


    public static void main (String[]args){
    }

}