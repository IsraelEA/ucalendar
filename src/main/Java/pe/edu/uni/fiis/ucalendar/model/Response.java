package pe.edu.uni.fiis.ucalendar.model;

public class Response<T> {
    Boolean coError;
    String deeError;
    T t;

    public Boolean getCoError() {
        return coError;
    }

    public void setCoError(Boolean coError) {
        this.coError = coError;
    }

    public String getDeeError() {
        return deeError;
    }

    public void setDeeError(String deeError) {
        this.deeError = deeError;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }
}
