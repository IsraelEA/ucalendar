package pe.edu.uni.fiis.ucalendar.dao.curso;

import pe.edu.uni.fiis.ucalendar.model.Curso;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CursoDaoImpl implements CursoDao{
    public Curso agregarcurso(Curso a, Connection b){
        try{
            StringBuffer sql= new StringBuffer();
            sql.append("insert into curso( nombre, idcurso,idalumno) values(").append("?,?,?)");
            PreparedStatement sentencia=b.prepareStatement(sql.toString());
            sentencia.setString(1,a.getNombre());
            sentencia.setString(2,a.getIdcurso());

        } catch (SQLException e) {
            e.printStackTrace();
        }
    return a;

    }

    @Override
    public List<Curso> mostrarcursos(Connection b) {
        List<Curso> lista= new ArrayList<Curso>();
        try{
            StringBuffer sql= new StringBuffer();
            sql.append("select nombre,idcurso,idalumno from curso");

            Statement sentencia=b.createStatement();
            ResultSet rs=sentencia.executeQuery(sql.toString());
            while(rs.next()){
                Curso c= new Curso();
                c.setNombre(rs.getString("nombre"));
                c.setIdcurso(rs.getString("idcurso"));
                c.setIdalumno(rs.getString("idalumno"));
                lista.add(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lista;
    }


}
