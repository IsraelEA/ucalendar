package pe.edu.uni.fiis.ucalendar.model;

public class Usuario {
    private String username;
    private String correo;
    private String idusuario;
    private String contraseña;

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(String idusuario) {
        this.idusuario = idusuario;
    }

    public Usuario(String username, String correo, String contraseña) {
        this.username = username;
        this.correo = correo;
        this.contraseña=contraseña;
    }

    public Usuario() {
    }
}
