var example2 = new Vue({
    el: '#index',
    data: {

        registrar:{
            visible: true,
            nombre: '',
            email: '',
            contraseña:''
        },
        login:{
            visible: false
        },
        calendario:{
            visible: false
        }
    },
    // define methods under the `methods` object
    methods: {
      registrarUsuario: function () {
        axios.post('/ucalendar2_war/registro-usuario',{
            username: this.registrar.nombre,
            correo: this.registrar.email,
            contraseña: this.registrar.contraseña
            }
        ).then(function (response) {
            console.log(response);
            if(response.data.coError){
                this.registrar.visible = false;
                this.calendario.visible = false;
                this.login.visible = true;
            }else{

            }
          })
          .catch(function (error) {
            console.log(error);
          });

          this.registrar.visible = false;
          this.calendario.visible = true;
          this.login.visible = true;

      }
    }
  });